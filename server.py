# -*- coding: utf-8 -*-
import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

# Cria o aplicativo
app = Flask(__name__)

# Altera a configuração do banco de dados (SQLite por enquanto)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///elementos.db'

# Diretório para upload de arquivos
app.config['UPLOAD_FOLDER'] = os.path.abspath("uploads")

@app.after_request
def cors(response):
	"""CORS"""
	methods = 'GET, POST, DELETE'
	headers = 'X-Requested-With, Content-Type'
	response.headers['Access-Control-Allow-Origin'] = '*'
	response.headers['Access-Control-Allow-Methods'] = methods
	response.headers['Access-Control-Allow-Headers'] = headers
	return response

@app.route('/')
def home():
	return 'Hello, world!'

# Importa as APIs
from api import *
from models import initialize
initialize()

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5000, debug=True)