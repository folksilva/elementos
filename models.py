# -*- coding: utf-8 -*-
import os
from hashlib import md5
from time import localtime
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import secure_filename
from server import app
from dateutil import parser

# Instancia o SQLAlchemy
db = SQLAlchemy(app)

def initialize():
	db.create_all()

class Elemento(db.Model):
	"""Um elemento do sistema"""
	id = db.Column(db.Integer, primary_key=True)
	titulo = db.Column(db.Unicode(200), nullable=False)
	tipo = db.Column(db.String(10), nullable=False)
	descricao = db.Column(db.UnicodeText)
	valor = db.Column(db.Float(2), default=0)

	def __init__(self, titulo, tipo, descricao, valor):
		"""Inicializa o elemento"""
		self.titulo = titulo
		self.tipo = tipo
		self.descricao = descricao
		self.valor = valor

	def dict(self):
		"""Retorna a representação em dicionário do elemento"""
		# Relacionamentos de dependencia desse elemento com outros
		outgoing_rel = [relacionamento.outgoing_dict() for relacionamento in self.outgoing_relationships]
		# Relacionamentos de dependencia de outros elementos com esse
		incoming_rel = [relacionamento.incoming_dict() for relacionamento in self.incoming_relationships]
		# Relacionamentos de propriedade desse elemento com outros
		outgoing_own = [relacionamento.outgoing_dict() for relacionamento in self.outgoing_ownerships]
		# Relacionamentos de propriedade de outros elementos com esse
		incoming_own= [relacionamento.incoming_dict() for relacionamento in self.incoming_ownerships]
		return dict(
			id=self.id,
			titulo=self.titulo,
			tipo=self.tipo,
			descricao=self.descricao,
			valor=self.valor,
			propriedades=[propriedade.dict() for propriedade in self.propriedades],
			relacionamentos=outgoing_rel + incoming_rel,
			pessoas=outgoing_own + incoming_own,
			anexos=[anexo.dict() for anexo in self.anexos],
			caminho=self.get_path()
		)

	def simple_dict(self):
		"""Retorna a representação simplificada do elemento em dicionário"""
		return dict(
			id=self.id,
			titulo=self.titulo,
			tipo=self.tipo,
			descricao=self.descricao,
			valor=self.valor
		)

	def get_path(self):
		"""Retorna o(s) caminho(s) recursivamente até esse elemento"""
		pais = [] # Um elemento pode ter um ou mais pais (Herança Múltipla)
		for relacionamento in self.outgoing_relationships:
			if relacionamento.tipo == 'child' and relacionamento.para.id != self.id:
				pais.append(relacionamento.para)
		for relacionamento in self.incoming_relationships:
			if relacionamento.tipo == 'parent' and relacionamento.de.id != self.id:
				pais.append(relacionamento.de)
		return [dict(elemento=parent.simple_dict(), caminho=parent.get_path()) for parent in pais]


	def add_propriedade(self, nome=None, valor=None, propriedade=None):
		"""Adiciona uma propriedade ao elemento"""
		if propriedade:
			propriedade.elemento_id = self.id
		else:
			propriedade = Propriedade(nome, valor, self)
		db.session.add(propriedade)
		db.session.commit()

	def add_anexo(self, file_storage=None, anexo=None):
		"""Adiciona um anexo ao elemento"""
		if anexo:
			anexo.elemento_id  = self.id
		else:
			anexo = Anexo(file_storage, self)
		db.session.add(anexo)
		db.session.commit()

	def add_relacionamento(self, tipo=None, para=None, relacionamento=None):
		"""Adiciona um relacionamento"""
		if relacionamento:
			relacionamento.de_id = self.id
		else:
			relacionamento = Relacionamento(para, tipo, self)
		db.session.add(relacionamento)
		db.session.commit()

	def add_pessoa(self, tipo=None, para=None, pessoa=None):
		"""Adiciona uma pessoa"""
		if pessoa:
			pessoa.de_id = self.id
		else:
			pessoa = Pessoa(para, tipo, self)
		db.session.add(pessoa)
		db.session.commit()


class Propriedade(db.Model):
	"""Uma propriedade de um elemento do sistema"""
	id = db.Column(db.Integer, primary_key=True)
	elemento_id = db.Column(db.ForeignKey('elemento.id'))
	nome = db.Column(db.Unicode(140), nullable=False)
	valor_string = db.Column(db.Unicode(200))
	valor_float = db.Column(db.Float)
	valor_int = db.Column(db.Integer)
	valor_date = db.Column(db.DateTime)
	valor_bool = db.Column(db.Boolean)
	tipo_dado = db.Column(db.String(20)) #str,float,int,datetime,bool
	elemento = db.relationship('Elemento', backref=db.backref('propriedades'))

	def __init__(self, nome, valor, elemento=None):
		"""Inicializa a propriedade"""
		if elemento:
			self.elemento_id = elemento.id
		self.nome = nome
		self.convert_value(valor)

	def dict(self):
		"""Retorna a representação em dicionário da propriedade"""
		valor = None
		if self.tipo_dado == 'str':
			valor = self.valor_string
		elif self.tipo_dado == 'float':
			valor = self.valor_float
		elif self.tipo_dado == 'int':
			valor = self.valor_int
		elif self.tipo_dado == 'date':
			valor = self.valor_date.isoformat()
		elif self.tipo_dado == 'bool':
			valor = self.valor_bool
		return dict(
			id=self.id,
			elemento_id=self.elemento_id,
			tipo=self.tipo_dado,
			nome=self.nome,
			valor=valor
		)

	def convert_value(self, value):
		"""Analisa o valor e determina o tipo de dado"""
		# É float?
		try:
			self.valor_float = float(value)
			self.tipo_dado = 'float'
		except ValueError:
			# É int?
			try:
				self.valor_int = int(value)
				self.tipo_dado = 'int'
			except ValueError:
				# É booleano?
				if value.lower() in ['true','sim','yes','verdadeiro','1','ok']:
					self.valor_bool = True
					self.tipo_dado = 'bool'
				elif value.lower() in ['false','não','no','falso','0','nao']:
					self.valor_bool = False
					self.tipo_dado = 'bool'
				else:
					# É uma data?
					try:
						self.valor_date = parser.parse(value)
						self.tipo_dado = 'date'
					except ValueError:
						# Então é uma string!
						self.valor_string = value
						self.tipo_dado = 'str'


class Anexo(db.Model):
	"""Um anexo de um elemento"""
	id = db.Column(db.Integer, primary_key=True)
	elemento_id = db.Column(db.ForeignKey('elemento.id'))
	filename = db.Column(db.Unicode(200))
	filepath = db.Column(db.Unicode(200))
	filesize = db.Column(db.Float())
	filetype = db.Column(db.String(100))
	elemento = db.relationship('Elemento', backref=db.backref('anexos'))

	def __init__(self, file_storage, elemento=None):
		"""Inicializa o anexo"""
		if elemento:
			self.elemento_id = elemento.id
		
		self.filename = secure_filename(file_storage.filename)
		self.filepath = "%s_%s" % (md5(str(localtime())).hexdigest(), self.filename)
		abspath = os.path.join(app.config['UPLOAD_FOLDER'], self.filepath)
		# Salva o arquivo
		file_storage.save(abspath)
		self.filesize = os.stat(abspath).st_size
		self.filetype = file_storage.mimetype

	def dict(self):
		"""Retorna a representação em dicionário do anexo"""
		return dict(
			id=self.id,
			elemento_id=self.elemento_id,
			filename=self.filename,
			filesize=self.filesize,
			filetype=self.filetype
		)


class Relacionamento(db.Model):
	"""Um relacionamento de dependencia entre elementos"""
	id = db.Column(db.Integer, primary_key=True)
	de_id = db.Column(db.ForeignKey('elemento.id'))
	para_id = db.Column(db.ForeignKey('elemento.id'))
	tipo = db.Column(db.String(15), default='dependent')
	de = db.relationship('Elemento', backref=db.backref('outgoing_relationships'), primaryjoin='Relacionamento.de_id == Elemento.id')
	para = db.relationship('Elemento', backref=db.backref('incoming_relationships'), primaryjoin='Relacionamento.para_id == Elemento.id')

	def __init__(self, para, tipo=None, de=None):
		"""Inicializa o relacionamento"""
		if de:
			self.de_id = de.id
		if tipo and tipo in ['parent', 'child', 'dependent', 'dependence', 'brother', 'clone']:
			self.tipo = tipo
		self.para_id = para

	def inverse_relationship(self, tipo=None):
		"""Inverte o tipo de relação para o outro lado"""
		if not tipo:
			tipo = self.tipo

		if tipo == 'parent': # Se este é pai o outro é filho
			return 'child'
		elif tipo == 'child': # Se este é filho o outro é pai
			return 'parent'
		elif tipo == 'dependent': # Se este é dependente o outro é dependência
			return 'dependence'
		elif tipo == 'dependence': # Se este é dependência o outro é dependente
			return 'dependent'
		else: # Se é irmão ou clone o outro também é
			return tipo

	def incoming_dict(self):
		"""Retorna a representação em dicionário de uma relação de entrada"""
		return dict(
			id=self.id,
			tipo=self.inverse_relationship(),
			elemento=self.de.simple_dict()
		)

	def outgoing_dict(self):
		"""Retorna a representação em dicionário de uma relação de saída"""
		return dict(
			id=self.id,
			tipo=self.tipo,
			elemento=self.para.simple_dict()
		)

class Pessoa(db.Model):
	"""Um relacionamento de dependencia entre elementos"""
	id = db.Column(db.Integer, primary_key=True)
	de_id = db.Column(db.ForeignKey('elemento.id'))
	para_id = db.Column(db.ForeignKey('elemento.id'))
	tipo = db.Column(db.String(15), default='resp_for')
	de = db.relationship('Elemento', backref=db.backref('outgoing_ownerships'), primaryjoin='Pessoa.de_id == Elemento.id')
	para = db.relationship('Elemento', backref=db.backref('incoming_ownerships'), primaryjoin='Pessoa.para_id == Elemento.id')

	def __init__(self, para, tipo=None, de=None):
		"""Inicializa o relacionamento"""
		if de:
			self.de_id = de.id
		if tipo and tipo in ['resp_from','resp_for','acco_from','acco_for','cons_from','cons_for','info_from','info_for']:
			self.tipo = tipo
		self.para_id = para

	def inverse_ownership(self, tipo=None):
		"""Inverte o tipo de relação para o outro lado"""
		if not tipo:
			tipo = self.tipo

		if tipo == 'resp_from': # Se este é responsabilidade o outro é responsável por ele
			return 'resp_for'
		elif tipo == 'resp_for': # Se este é responsável o outro é responsabilidade dele
			return 'resp_from'

		elif tipo == 'acco_from': # Se este é aprovado o outro é aprovador
			return 'acco_for'
		elif tipo == 'acco_for': # Se este é aprovador o outro é aprovado
			return 'acco_from'

		elif tipo == 'cons_from': # Se este é consultado o outro é consultor
			return 'cons_for'
		elif tipo == 'cons_for': # Se este é consultor o outro é consultado
			return 'cons_from'

		elif tipo == 'info_from': # Se este é informado o outro é informante
			return 'info_for'
		elif tipo == 'info_for': # Se este é informante o outro é informado
			return 'info_from'

	def incoming_dict(self):
		"""Retorna a representação em dicionário de uma relação de entrada"""
		return dict(
			id=self.id,
			tipo=self.inverse_ownership(),
			elemento=self.de.simple_dict()
		)

	def outgoing_dict(self):
		"""Retorna a representação em dicionário de uma relação de saída"""
		return dict(
			id=self.id,
			tipo=self.tipo,
			elemento=self.para.simple_dict()
		)

