# -*- coding: utf-8 -*-
import os
import math
from flask import jsonify, request, send_from_directory
from werkzeug.exceptions import NotFound
from server import app
from models import *

@app.route('/api/search', methods=['GET'])
def search_service():
	"""Faz uma busca por elementos"""
	term = request.args.get('term', None)
	if not term:
		return jsonify(message='Digite um termo para pesquisar')
	page = int(request.args.get('page', 1)) # Página atual
	limit = int(request.args.get('limit', 5)) # Limite de itens por página
	exclude = int(request.args.get('exclude', None)) # Ignorar esse elemento
	offset = (page - 1) * limit # Itens para pular
	if exclude:
		query = Elemento.query.filter(Elemento.titulo.like('%%%s%%' % term), Elemento.id != exclude)
	else:
		query = Elemento.query.filter(Elemento.titulo.like('%%%s%%' % term))
	total = query.count()
	num_pages = math.ceil(float(total) / float(limit))
	results = query.offset(offset).limit(limit).all()
	return jsonify(
		results=[e.simple_dict() for e in results],
		total=total,
		page=page,
		num_pages=int(num_pages)
	)



@app.route('/api/elemento', methods=['GET', 'POST'])
@app.route('/api/elemento/<int:elemento_id>', methods=['GET', 'POST', 'DELETE'])
def elemento_service(elemento_id=None):
	"""Permite obter, atualizar, criar e excluir elementos"""
	if elemento_id:
		elemento = Elemento.query.get(elemento_id)
		if not elemento:
			raise NotFound()

		if request.method == 'POST':
			elemento.titulo = request.json['titulo']
			elemento.tipo = request.json['tipo']
			elemento.descricao = request.json['descricao']
			elemento.valor = float(request.json['valor'])

			db.session.add(elemento)
			db.session.commit()

		elif request.method == 'DELETE':
			# Deletar todas as propriedades do elemento
			for propriedade in elemento.propriedades:
				db.session.delete(propriedade)
			# Deletar todos os anexos do elemento
			for anexo in elemento.anexos:
				os.remove(os.path.join(app.config['UPLOAD_FOLDER'], anexo.filepath))
				db.session.delete(anexo)
			# Deletar todos os relacionamentos de dependencia do elemento
			for relacionamento in elemento.incoming_relationships + elemento.outgoing_relationships:
				db.session.delete(relacionamento)
			# Deletar todos os relacionamentos de propriedade do elemento
			for pessoa in elemento.incoming_ownerships + elemento.outgoing_ownerships:
				db.session.delete(pessoa)
			# Deletar o elemento
			db.session.delete(elemento)
			db.session.commit()
			return 'OK'
		
		return jsonify(**elemento.dict())

	elif request.method == 'GET':
		elemento = Elemento.query.order_by(Elemento.id).first_or_404()
		return jsonify(**elemento.dict())

	elif request.method == 'POST':
		titulo = request.json['titulo']
		tipo = request.json['tipo']
		descricao = request.json['descricao']
		valor = float(request.json['valor'])
		elemento = Elemento(titulo, tipo, descricao, valor)
		
		db.session.add(elemento)
		db.session.commit()

		return jsonify(**elemento.dict())


@app.route('/api/elemento/<int:elemento_id>/propriedades', methods=['POST'])
@app.route('/api/elemento/<int:elemento_id>/propriedades/<int:propriedade_id>', methods=['POST','DELETE'])
def propriedade_service(elemento_id, propriedade_id=None):
	"""Permite criar, atualizar ou excluir propriedades de elementos"""
	elemento = Elemento.query.get(elemento_id)
	if not elemento:
		raise NotFound()

	if propriedade_id:
		propriedade = Propriedade.query.get(propriedade_id)
		if not propriedade:
			raise NotFound()

		if request.method == 'POST':
			propriedade.nome = request.json['nome']
			propriedade.convert_value(request.json['valor'])
			db.session.add(propriedade)
			db.session.commit()
			return jsonify(propriedade=propriedade.dict())

		else:
			db.session.delete(propriedade)
			db.session.commit()
			return 'OK'

	else:
		nome = request.json['nome']
		valor = request.json['valor']
		propriedade = Propriedade(nome,valor)
		elemento.add_propriedade(propriedade=propriedade)
		return jsonify(propriedade=propriedade.dict())


@app.route('/api/elemento/<int:elemento_id>/anexos', methods=['POST'])
@app.route('/api/elemento/<int:elemento_id>/anexos/<int:anexo_id>', methods=['GET','DELETE'])
def anexo_service(elemento_id, anexo_id=None):
	"""Permite adicionar, baixar e remover anexos"""
	elemento = Elemento.query.get(elemento_id)
	if not elemento:
		raise NotFound()

	if anexo_id:
		anexo = Anexo.query.get(anexo_id)
		if not anexo or not os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], anexo.filepath)):
			raise NotFound()

		if request.method == 'GET':
			return send_from_directory(app.config['UPLOAD_FOLDER'], anexo.filepath, as_attachment=True, attachment_filename=anexo.filename)
		else:
			os.remove(os.path.join(app.config['UPLOAD_FOLDER'], anexo.filepath))
			db.session.delete(anexo)
			db.session.commit()
			return 'OK'
	else:
		upload = request.files['upload']
		anexo = Anexo(upload)
		print anexo.dict()
		elemento.add_anexo(anexo=anexo)
		return jsonify(anexo=anexo.dict())

@app.route('/api/elemento/<int:elemento_id>/relacionamentos', methods=['POST'])
@app.route('/api/elemento/<int:elemento_id>/relacionamentos/<int:relacionamento_id>', methods=['POST','DELETE'])
def relacionamento_service(elemento_id, relacionamento_id=None):
	"""Permite criar, atualizar ou excluir um relacionamento de dependência do elemento"""
	elemento = Elemento.query.get(elemento_id)
	if not elemento:
		raise NotFound()

	if relacionamento_id:
		relacionamento = Relacionamento.query.get(relacionamento_id)
		if not relacionamento:
			raise NotFound()

		if request.method == 'POST': # Atualizar o relacionamento
			if relacionamento.de_id == elemento.id:
				relacionamento.para_id = int(request.json['para_id'])
				relacionamento.tipo = request.json['tipo']
			else:
				relacionamento.de_id =  int(request.json['para_id'])
				relacionamento.tipo = relacionamento.inverse_relationship(request.json['tipo'])
			db.session.add(relacionamento)
			db.session.commit()
			return jsonify(relacionamento=relacionamento.outgoing_dict())
		else: # Excluir o relacionamento
			db.session.delete(relacionamento)
			db.session.commit()
			return 'OK'
	else:
		para = int(request.json['para_id'])
		tipo = request.json['tipo']
		relacionamento = Relacionamento(para, tipo)
		elemento.add_relacionamento(relacionamento=relacionamento)
		return jsonify(relacionamento=relacionamento.outgoing_dict())

@app.route('/api/elemento/<int:elemento_id>/pessoas', methods=['POST'])
@app.route('/api/elemento/<int:elemento_id>/pessoas/<int:pessoa_id>', methods=['POST','DELETE'])
def pessoa_service(elemento_id, pessoa_id=None):
	"""Permite criar, atualizar ou excluir um relacionamento de propriedade do elemento"""
	elemento = Elemento.query.get(elemento_id)
	if not elemento:
		raise NotFound()

	if pessoa_id:
		pessoa = Pessoa.query.get(pessoa_id)
		if not pessoa:
			raise NotFound()

		if request.method == 'POST': # Atualizar o relacionamento
			if pessoa.de_id == elemento.id:
				pessoa.para_id = int(request.json['para_id'])
				pessoa.tipo = request.json['tipo']
			else:
				pessoa.de_id =  int(request.json['para_id'])
				pessoa.tipo = pessoa.inverse_ownership(request.json['tipo'])
			db.session.add(pessoa)
			db.session.commit()
			return jsonify(pessoa=pessoa.outgoing_dict())
		else: # Excluir o relacionamento
			db.session.delete(pessoa)
			db.session.commit()
			return 'OK'
	else:
		para = int(request.json['para_id'])
		tipo = request.json['tipo']
		pessoa = Pessoa(para, tipo)
		elemento.add_pessoa(pessoa=pessoa)
		return jsonify(pessoa=pessoa.outgoing_dict())
