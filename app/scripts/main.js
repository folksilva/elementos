'use strict';
var app = angular.module('elements', ['ngResource','ngRoute','ui.select2']);
var serverApiURI = 'http://localhost:5000:5000/api/';
var serverApiURISingle = 'http://localhost:5000/api/';

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: 'partials/element.html',
		controller: 'ElementoCtrl'
	}).
	when('/:elementoId', {
		templateUrl: 'partials/element.html',
		controller: 'ElementoCtrl'
	}).
	otherwise({redirectTo: '/'});
}]);
