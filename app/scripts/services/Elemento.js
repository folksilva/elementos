'use strict';

app.factory('Elemento', ['$resource', function($resource) {

	return $resource(serverApiURI + 'elemento/:id', {id:'@id'},{
		'get': {
			method: 'GET'
		},
		'save': {
			method: 'POST'
		},
		'delete': {
			method: 'DELETE',
			headers : {
				'Content-Type' : 'application/json'
			}
		}
	});

}]);