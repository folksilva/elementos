'use strict';

app.factory('Anexo', ['$resource', function($resource) {

	return $resource(serverApiURI + 'elemento/:elemento_id/anexos/:id', {elemento_id:'@elemento_id',id:'@id'},{
		'download': {
			method: 'GET'
		},
		'upload': {
			method: 'POST'
		},
		'delete': {
			method: 'DELETE',
			headers : {
				'Content-Type' : 'application/json'
			}
		}
	});

}]);