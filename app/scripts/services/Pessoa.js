'use strict';

app.factory('Pessoa', ['$resource', function($resource) {

	return $resource(serverApiURI + 'elemento/:elemento_id/pessoas/:id', {elemento_id:'@elemento_id',id:'@id'},{
		'get': {
			method: 'GET'
		},
		'save': {
			method: 'POST'
		},
		'delete': {
			method: 'DELETE',
			headers : {
				'Content-Type' : 'application/json'
			}
		}
	});

}]);