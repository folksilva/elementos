'use strict';
/**
 * Controla a lista de pessoas
 */
app.controller('PessoasCtrl', ['$scope','Pessoa',function($scope,Pessoa) {
	$scope.relacionamentos = [
		{label:'É responsabilidade de',value:'resp_from'},
		{label:'É responsável por',value:'resp_for'},
		{label:'É aprovado por',value:'acco_from'},
		{label:'É aprovador de',value:'acco_for'},
		{label:'É consultado por',value:'cons_from'},
		{label:'É consultor de',value:'cons_for'},
		{label:'É informado para',value:'info_from'},
		{label:'É informado de',value:'info_for'}
	];
	$scope.getNomeRelacionamento = function(relacionamento) {
		for (var i = 0, tipo; tipo = $scope.relacionamentos[i]; i++) {
			if (tipo.value === relacionamento) {
				return tipo.label;
			}
		}
	};
	$scope.addPessoa = function(){
		$scope.novaPessoa.elemento_id = $scope.elemento.id;
		$scope.novaPessoa.para_id = $scope.novaPessoa.elemento.id;
		$('.header h1').addClass('busy');
		Pessoa.save($scope.novaPessoa, function(pes){
			$scope.elemento.pessoas.push(pes.pessoa);
			$scope.novaPessoa.tipo = null;
			$scope.novaPessoa.nome = null;
			$('#novaPessoaTipo').select2("focus");
			$('.header h1').removeClass('busy');
		});
	};
	$scope.savePessoa = function(index){
		var pessoa = $scope.elemento.pessoas[index];
		pessoa.elemento_id = $scope.elemento.id;
		pessoa.para_id = pessoa.elemento.id;
		console.log(pessoa);
		$('.header h1').addClass('busy');
		Pessoa.save(pessoa, function(){$('.header h1').removeClass('busy');});
	};
	$scope.removePessoa = function(index){
		$('.header h1').addClass('busy');
		Pessoa.delete({
			elemento_id:$scope.elemento.id,
			id:$scope.elemento.pessoas[index].id
		}, function(){
			$scope.elemento.pessoas.splice(index,1);
			$('.header h1').removeClass('busy');
		});
	};
}]);