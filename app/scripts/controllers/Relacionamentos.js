'use strict';
/**
 * Controla a lista de relacionamentos
 */
app.controller('RelacionamentosCtrl', ['$scope','Relacionamento',function($scope,Relacionamento) {
	$scope.relacionamentos = [
		{label:'É pai de',value:'parent'},
		{label:'É filho de',value:'child'},
		{label:'Depende de',value:'dependent'},
		{label:'É dependência para',value:'dependence'},
		{label:'É irmão de',value:'brother'},
		{label:'É clone de',value:'clone'}
	];

	$scope.getNomeRelacionamento = function(relacionamento) {
		for (var i = 0, tipo; tipo = $scope.relacionamentos[i]; i++) {
			if (tipo.value === relacionamento) {
				return tipo.label;
			}
		}
	};
	$scope.addRelacionamento = function(){
		$scope.novoRelacionamento.elemento_id = $scope.elemento.id;
		$scope.novoRelacionamento.para_id = $scope.novoRelacionamento.elemento.id;
		$('.header h1').addClass('busy');
		Relacionamento.save($scope.novoRelacionamento, function(rel){
			$scope.elemento.relacionamentos.push(rel.relacionamento);
			$scope.novoRelacionamento.tipo = null;
			$scope.novoRelacionamento.elemento = null;
			$('#novoRelacionamentoTipo').select2("focus");
			$('.header h1').removeClass('busy');
		});
		
	};
	$scope.saveRelacionamento = function(index){
		var relacionamento = $scope.elemento.relacionamentos[index];
		relacionamento.elemento_id = $scope.elemento.id;
		relacionamento.para_id = relacionamento.elemento.id;
		$('.header h1').addClass('busy');
		Relacionamento.save(relacionamento, function(){$('.header h1').removeClass('busy');});
	};
	$scope.removeRelacionamento = function(index){
		$('.header h1').addClass('busy');
		Relacionamento.delete({
			elemento_id:$scope.elemento.id,
			id:$scope.elemento.relacionamentos[index].id
		}, function(){
			$scope.elemento.relacionamentos.splice(index,1);
			$('.header h1').removeClass('busy');
		});
	};
}]);