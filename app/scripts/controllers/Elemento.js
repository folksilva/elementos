'use strict';
/**
 * Controla o elemento
 */
app.controller('ElementoCtrl', ['$scope','$filter','$routeParams','$location','KeyboardManager','Elemento','Relacionamento',function($scope,$filter,$routeParams,$location,KeyboardManager,Elemento,Relacionamento) {
	$scope.editMode = false;
	$scope.geralEditMode = false;
	$scope.propriedadesEditMode = false;
	$scope.anexosEditMode = false;
	$scope.relacionamentosEditMode = false;
	$scope.pessoasEditMode = false;
	$scope.path = [];
	var path = [];
	$scope.tipos = [{
		label:'Serviço',
		value:'service'
	},{
		label:'Componente',
		value:'component'
	},{
		label:'Pessoa',
		value:'people'
	}];
	$scope.elementoSelector = {
		placeholder: 'Encontre um elemento',
		minimumInputLength: 2,
		ajax: {
			url: serverApiURISingle + 'search',
			dataType: 'json',
			quietMillis: 100,
			data: function (term, page) {
				return {
					term: term,
					limit: 3,
					page: page,
					exclude: $scope.elemento.id
				};
			},
			results: function(data, page) {
				var more = page < data.num_pages;
				return {results: data.results, more:more}
			}
		},
		initSelection: function(el, callback) {
			console.log($(el).val());
		},
		formatResult: function(elemento) {
			var template = '<div>';
			switch(elemento.tipo) {
				case 'service':
					template += '<span class="pull-right label label-primary">Serviço</span>';
					break;
				case 'component':
					template += '<span class="pull-right label label-warning">Componente</span>';
					break;
				case 'people':
					template += '<span class="pull-right label label-danger">Pessoa</span>';
					break;
			}
			template += '<strong>' + elemento.titulo + '</strong>';
			var descricao = elemento.descricao.substr(0, 50);
			if (elemento.descricao.length > 50) {
				descricao += '...'
			}
			template += '<div class="text-muted"><small>' + descricao + '</small></div>';
			template += '</div>';
			return template;
		},
		formatSelection: function(elemento) {
			return elemento.titulo;
		}
	};
	// Retorna o primeiro caminho possível para o elemento
	function getSinglePath(step) {
		if (step) {
			path.push(step);
			if (step.caminho.length > 0) {
				getSinglePath(step.caminho[0]);
			}
		}
	}
	// Cria um novo elemento relacionado ao atual
	function createElemento(relacionamento) {
		var novoElemento = {
			titulo:'Novo Elemento',
			descricao:'Descreva aqui o que é esse elemento',
			tipo: $scope.elemento.tipo,
			valor: 0
		};
		var relacionamento = {
			elemento_id:$scope.elemento.id,
			tipo:relacionamento
		};
		Elemento.save(novoElemento, function(el) {
			relacionamento.para_id = el.id;
			Relacionamento.save(relacionamento, function(){
				$location.path('/' + el.id);
			});
		});
	}
	// Carrega o elemento inicial
	$('.header h1').addClass('busy');
	$scope.elemento = Elemento.get({id:$routeParams.elementoId},function(){
		// Percorrer as propriedades e formatar o valor
		for (var i = 0; i < $scope.elemento.propriedades.length; i++) {
			switch ($scope.elemento.propriedades[i].tipo) {
				case 'date':
					$scope.elemento.propriedades[i].valor = $filter('date')(new Date($scope.elemento.propriedades[i].valor),'dd/MM/yyyy HH:mm');
					break;
				case 'bool':
					if ($scope.elemento.propriedades[i].valor) {
						$scope.elemento.propriedades[i].valor = 'SIM'
					} else {
						$scope.elemento.propriedades[i].valor = 'NÃO'
					}
					break;
			}
		}
		// Percorrer os anexos, adicionar link de download e formatar tamanho
		for (var i = 0, anexo; anexo = $scope.elemento.anexos[i]; i++) {
			$scope.elemento.anexos[i].url = serverApiURISingle + 'elemento/' +
				$scope.elemento.id +
				'/anexos/' + anexo.id;
			var bites = anexo.filesize;
			var unit = 'B';
			if (bites > 1024) {
				bites = bites / 1024;
				unit = 'KB';
				if (bites > 1024) {
					bites = bites / 1024;
					unit = 'MB';
				}
			}
			$scope.elemento.anexos[i].size = {bites:bites, unit:unit};
		}
		// Percorrer o caminho
		path = [{elemento:$scope.elemento}];
		getSinglePath($scope.elemento.caminho[0]);
		$scope.path = path.reverse();
		$('.header h1').removeClass('busy');
		if ($scope.elemento.titulo === 'Novo Elemento') {
			$scope.editMode = true;
		}
	},function(){
		if ($routeParams.elementoId) {
			$location.path('/');
		}
		$scope.elemento = {
			titulo: 'Elementos',
			tipo: 'service',
			descricao: 'Tudo começa aqui, adicione dependências e relacionamentos para navegar.',
			valor: 0,
			caminho:[],
			propriedades: [{nome:'Versão',valor:'0.0.1'},{nome:'Plataforma',valor:'Python + JS'},{nome:'Banco de dados',valor:'SQLite'}],
			anexos: [],
			relacionamentos: [],
			pessoas: []
		};
		$('.header h1').removeClass('busy');
	});

	// Navega para um elemento a partir da busca
	$scope.jumpToElement = function(){
		$location.path('/' + $scope.search.id);
	}

	// Salva o elemento
	$scope.save = function(){
		$('.header h1').addClass('busy');
		Elemento.save($scope.elemento, function(e){
			$('.header h1').removeClass('busy');
			$scope.elemento.id = e.id;
		});
	};

	$scope.newElement = function(){
		createElemento('parent');
	};

	$scope.beginDelete = function(){
		var num1 = Math.floor(Math.random() * 101);
		var num2 = Math.floor(Math.random() * 101);
		$scope.respostaEsperada = num1 + num2;
		$scope.expressao = num1 + ' + ' + num2;
		$scope.resposta = null;
		$scope.mensagem = null;
		$('#confirmDelete').collapse('show');
		$('#confirmDeleteResposta').focus();
	};

	// Exclui o elemento
	$scope.delete = function(){
		var mensagens = [
			'Opss!!! A resposta não está certa, tente novamente.',
			'Opss!!! O número está errado, tente de novo.',
			'Opss!!! Isso não deu certo, tente outra vez.'
		];
		if ($scope.resposta && Number($scope.resposta) === $scope.respostaEsperada){
			$('#confirmDelete').collapse('hide');
			$('.header h1').addClass('busy');
			Elemento.delete($scope.elemento, function(){
				window.location.reload();
			});
		} else {
			$scope.beginDelete();
			$scope.mensagem = mensagens[Math.floor(Math.random() * 3)];
		}
	};

	// Observa o modo de edição
	$scope.$watch('editMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#geralEdit').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('input#titulo').focus();
				});
			},10);
		}
	});
	$scope.$watch('geralEditMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#geralEdit').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('input#titulo').focus();
				});
			},10);
		}
	});
	$scope.$watch('propriedadesEditMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#PropriedadesPanel').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('#PropriedadesPanel td input:first').focus();	
				});
			},10);
		}
	});
	$scope.$watch('anexosEditMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#AnexosPanel').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('#AnexosPanel td input:first').focus();	
				});
			},10);
		}
	});
	$scope.$watch('relacionamentosEditMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#RelacionamentosPanel').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('#RelacionamentosPanel td select:first').select2('focus');	
				});
			},10);
		}
	});
	$scope.$watch('pessoasEditMode', function(v){
		if (v) {
			setTimeout(function(){
				var top = $('#PessoasPanel').offset().top;
				top -= $('.header').outerHeight() + 20;
				$('html,body').animate({scrollTop: top}, 600, function(){
					$('#PessoasPanel td select:first').select2('focus');	
				});
			},10);
		}
	});

	/* Atalhos de teclado */
	// Alterna a edição
	KeyboardManager.bind('e', function(){
		$scope.editMode = !$scope.editMode;
	},{inputDisabled:true});
	
	// Põe o foco na busca
	KeyboardManager.bind('f', function(){
		$('#search').select2("focus");
	},{inputDisabled:true});
	
	// Mostra a ajuda de atalhos
	KeyboardManager.bind('h', function(){
		$('#help').collapse('toggle');
	},{inputDisabled:true});
	
	// Cria elemento filho
	KeyboardManager.bind('shift+n', function(){
		console.log('Novo elemento filho');
		createElemento('parent');
	},{inputDisabled:true});
	
	// Cria elemento dependente
	KeyboardManager.bind('shift+d', function(){
		console.log('Novo elemento dependente');
		createElemento('dependence');
	},{inputDisabled:true});

	// Exclui o elemento
	KeyboardManager.bind('shift+delete', function(){
		$scope.beginDelete();
	});

	// Põe foco no geral
	KeyboardManager.bind('0', function(){
		var top = 0;
		if ($scope.editMode || $scope.geralEditMode) {
			top = $('#geralEdit').offset().top;
		} else {
			top = $('#geralView').offset().top;
		}
		top -= $('.header').outerHeight() + 20;
		$('html,body').animate({scrollTop: top}, 600);
	},{inputDisabled:true});

	// Edita o geral
	KeyboardManager.bind('shift+0', function(){
		$scope.geralEditMode = !$scope.geralEditMode;
	},{inputDisabled:true});

	// Põe foco nas propriedades
	KeyboardManager.bind('1', function(){
		var top = $('#PropriedadesPanel').offset().top;
		top -= $('.header').outerHeight() + 20;
		$('html,body').animate({scrollTop: top}, 600);
	},{inputDisabled:true});

	// Edita propriedades
	KeyboardManager.bind('shift+1', function(){
		$scope.propriedadesEditMode = !$scope.propriedadesEditMode;
	},{inputDisabled:true});

	// Põe foco nos anexos
	KeyboardManager.bind('2', function(){
		var top = $('#PropriedadesPanel').offset().top;
		top -= $('.header').outerHeight() + 20;
		$('html,body').animate({scrollTop: top}, 600);
	},{inputDisabled:true});

	// Edita anexos
	KeyboardManager.bind('shift+2', function(){
		$scope.anexosEditMode = !$scope.anexosEditMode;
	},{inputDisabled:true});

	// Põe foco nos relacionamento
	KeyboardManager.bind('3', function(){
		var top = $('#PropriedadesPanel').offset().top;
		top -= $('.header').outerHeight() + 20;
		$('html,body').animate({scrollTop: top}, 600);
	},{inputDisabled:true});

	// Edita relacionamentos
	KeyboardManager.bind('shift+3', function(){
		$scope.relacionamentosEditMode = !$scope.relacionamentosEditMode;
	},{inputDisabled:true});

	// Põe foco nas pessoas
	KeyboardManager.bind('4', function(){
		var top = $('#PropriedadesPanel').offset().top;
		top -= $('.header').outerHeight() + 20;
		$('html,body').animate({scrollTop: top}, 600);
	},{inputDisabled:true});

	// Edita pessoas
	KeyboardManager.bind('shift+4', function(){
		$scope.pessoasEditMode = !$scope.pessoasEditMode;
	},{inputDisabled:true});

	// Sai da edição, ajuda ou confirmação de exclusão ao pressionar ESC
	KeyboardManager.bind('esc', function(){
		if($('#confirmDelete').hasClass('in')){
			$('#confirmDelete').collapse('hide');
		}
		if($('#help').hasClass('in')){
			$('#help').collapse('hide');
		}
		$scope.editMode = false;
		$scope.geralEditMode = false;
		$scope.propriedadesEditMode = false;
		$scope.anexosEditMode = false;
		$scope.relacionamentosEditMode = false;
		$scope.pessoasEditMode = false;
		$('*:focus').blur();

	});
	$(window).scroll(function(){
		var top = document.body.scrollTop;
		var height = $('#mainBody').offset().top - $('.header').offset().top + 18;
		if (top > height) {
			$('.header').addClass('shadow');
		} else {
			$('.header').removeClass('shadow');
		}
	});

}]);