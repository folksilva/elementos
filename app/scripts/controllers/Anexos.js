'use strict';
/**
 * Controla a lista de anexos
 */
app.controller('AnexosCtrl', ['$scope', 'Anexo', function($scope, Anexo) {
	$scope.dropAreaClass = '';
	var droparea = document.getElementById("AnexosPanel");

	// Ao entrar ou sair da area de arrasto
	function dragEnterLeave(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		$scope.$apply(function(){
			$scope.dropAreaClass = '';
		});
	}

	// Ao mover sobre a area de arrasto
	function dragOver(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') > -1;
		$scope.$apply(function(){
			$scope.dropAreaClass = ok ? 'drop-over' : 'drop-invalid';
		});
	}

	// Ao soltar sobre a area de arrasto
	function drop(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		$scope.$apply(function(){$scope.dropAreaClass = '';});
		var files = evt.dataTransfer.files;
		if (files.length > 0) {
			$scope.$apply(function(){
				$scope.uploadFiles = [];
				for (var i = 0; i < files.length; i++) {
					$scope.uploadFiles.push(files[i]);
					upload(i);
				}
			});
		}
	}

	// Adicionar os ouvintes de eventos
	droparea.addEventListener('dragenter', dragEnterLeave, false);
	droparea.addEventListener('dragleave', dragEnterLeave, false);
	droparea.addEventListener('dragover', dragOver, false);
	droparea.addEventListener('drop', drop, false);

	// Faz o upload do arquivo
	function upload(index) {
		return (function(index){
			var data = new FormData();
			data.append('upload', $scope.uploadFiles[index]);
			var xhr = new XMLHttpRequest();
			xhr.upload.addEventListener('progress', function(evt) {
				$scope.$apply(function(){
					if (evt.lengthComputable) {
						$scope.uploadFiles[index].progress = Math.round(evt.loaded * 100 / evt.total);
					}
				});
			}, false);
			xhr.addEventListener('load', function(evt) {
				$scope.$apply(function(){
					$scope.uploadFiles[index].complete = true;;
					var upload = JSON.parse(evt.target.responseText);
					upload.anexo.url = serverApiURISingle + 'elemento/' +
						$scope.elemento.id +
						'/anexos/' + upload.anexo.id;
					var bites = upload.anexo.filesize;
					var unit = 'B';
					if (bites > 1024) {
						bites = bites / 1024;
						unit = 'KB';
						if (bites > 1024) {
							bites = bites / 1024;
							unit = 'MB';
						}
					}
					upload.anexo.size = {bites:bites, unit:unit};
					$scope.elemento.anexos.push(upload.anexo);
				});
			}, false);
			xhr.addEventListener('error', function(evt){
				$scope.$apply(function(){
					$scope.uploadFiles[index].failed = true;
					$scope.uploadFiles[index].progress = 0;
				});
			}, false);
			xhr.addEventListener('abort', function(evt){
				$scope.$apply(function(){
					$scope.uploadFiles[index].aborted = true;
					$scope.uploadFiles[index].progress = 0;
				});
			}, false);
			xhr.open('POST', serverApiURISingle + 'elemento/' + $scope.elemento.id + '/anexos');
			xhr.send(data);
		})(index);
	}

	$scope.addAnexo = function(e){
		$scope.$apply(function($scope) {
			$scope.uploadFiles = [];
			console.log(e.files);
			for (var i = 0; i < e.files.length; i++) {
				$scope.uploadFiles.push(e.files[i]);
				upload(i);
			}
		});
	};

	$scope.removeAnexo= function(index){
		$('.header h1').addClass('busy');
		Anexo.delete({
			elemento_id:$scope.elemento.id,
			id:$scope.elemento.anexos[index].id
		}, function(){
			$scope.elemento.anexos.splice(index,1);
			$('.header h1').removeClass('busy');
		});
	};
}]);