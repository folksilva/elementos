'use strict';
/**
 * Controla a lista de propriedades
 */
app.controller('PropriedadesCtrl', ['$scope','Propriedade',function($scope, Propriedade) {
	$scope.addPropriedade = function(){
		$scope.novaPropriedade.elemento_id = $scope.elemento.id;
		$('.header h1').addClass('busy');
		Propriedade.save($scope.novaPropriedade, function(prop){
			$scope.elemento.propriedades.push(prop.propriedade);
			$scope.novaPropriedade.nome = null;
			$scope.novaPropriedade.valor = null;
			$('#novaPropriedadeNome').focus();
			$('.header h1').removeClass('busy');
		});
	};
	$scope.savePropriedade  = function(index){
		$('.header h1').addClass('busy');
		Propriedade.save($scope.elemento.propriedades[index], function(){$('.header h1').removeClass('busy');});
	};
	$scope.removePropriedade = function(index){
		$('.header h1').addClass('busy');
		Propriedade.delete({
			elemento_id:$scope.elemento.id,
			id:$scope.elemento.propriedades[index].id
		}, function(){
			$scope.elemento.propriedades.splice(index,1);
			$('.header h1').removeClass('busy');
		});
		
	};
}]);